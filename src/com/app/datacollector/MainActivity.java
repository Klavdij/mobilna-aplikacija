package com.app.datacollector;

import java.util.Date;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.net.TrafficStats;
import android.net.Uri;
import android.os.Bundle;
import android.provider.CallLog;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.view.Menu;
import android.view.Window;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

public class MainActivity extends Activity {
	
	private long sendBytesNew;
	private String userId;
	private String emso;
	JSONObject user;
	JSONArray users;
	private String[] usersArray;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().requestFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_main);
		
		try{
			
			GetUsers gu=new GetUsers();
			gu.execute();
			
			users=new JSONArray(gu.get());
			usersArray=new String[users.length()];
			
			for (int i = 0; i < users.length(); i++) {
				user=(JSONObject)users.get(i);
				usersArray[i]=user.getString("imePrimek");
			}
			
		} catch (Exception e) {
			showMessage("Napaka (98) pri prejemanju podatkov.");
			e.printStackTrace();
		}
		
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
	    builder.setTitle("Izberi uporabnika:");
	    builder.setItems(usersArray, new DialogInterface.OnClickListener() {
	               public void onClick(DialogInterface dialog, int which) {
	            	   try{
	            		   user=(JSONObject)users.get(which);
	            		   
	            		   emso=user.getString("emso");
	            		   userId=user.getString("id");
	            		   
	            		   continueActivity();
	            		   
	            	   } catch (Exception e) {
	            		   showMessage("Napaka (99) pri izbiri uporabnika.");
	            		   e.printStackTrace();
	            	   }
	           }
	    });
	    
	    AlertDialog dialog = builder.create();
	    dialog.show();
		
	}
	
	private void continueActivity(){
		
		JSONObject podatki=getData();
		
		try {
		
			RestClient rc=new RestClient();
			rc.setData(podatki, emso);
			rc.execute();
			
			String response=rc.get();
			JSONObject json = new JSONObject(response);
			
			String result=json.getString("result");
			
			if(result.equals("success")){
				showMessage("Podatki uspe�no poslani.");
				setValue("datum",new Date().getTime());
				setValue("net",sendBytesNew);
			}
			else{
				showMessage("Napaka (100) pri sprejemanju odgovora.");
			}
			
			
		} catch (Exception e) {
			showMessage("Napaka (101) pri po�iljanju podatkov.");
			e.printStackTrace();
		}
		
		WebView myWebView = (WebView) findViewById(R.id.webview);
		myWebView.loadUrl("http://10.0.2.2:8080/Streznik/index.jsp?u="+userId);
		myWebView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }
        });
		// Vklopimo Javascript
		WebSettings webSettings = myWebView.getSettings();
		webSettings.setJavaScriptEnabled(true);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	private JSONObject getData(){
		JSONObject podatki=new JSONObject();
		
		JSONArray list=new JSONArray();		
		
		try {
			
			getCallData(list);
			getSMSMMSData(list);
			getNETData(list);
			
			podatki.put("podatki", list);
		} catch (JSONException e) {
			showMessage("Napaka (102) pri zbiranju podatkov.");
			e.printStackTrace();
		}
			
		return podatki;
		
	}
	
	private void getCallData(JSONArray list){
		
		try{
			long lastSendDate=getValue("datum");
			
			Cursor managedCursor = managedQuery(CallLog.Calls.CONTENT_URI, null,"date>?", new String[] {lastSendDate+""}, null);
			
	        int number = managedCursor.getColumnIndex(CallLog.Calls.NUMBER);
	        int type = managedCursor.getColumnIndex(CallLog.Calls.TYPE);
	        int date = managedCursor.getColumnIndex(CallLog.Calls.DATE);
	        int duration = managedCursor.getColumnIndex(CallLog.Calls.DURATION);
	        
	        while (managedCursor.moveToNext()) {
	            
	        	String phNumber = managedCursor.getString(number);
	            long callDate = Long.valueOf(managedCursor.getString(date));
	            long callDuration =Long.valueOf( managedCursor.getString(duration));
	            
	            String callType = managedCursor.getString(type);
	            int dir = -1;
	            int dircode = Integer.parseInt(callType);
	            
	            switch (dircode) {
	            case CallLog.Calls.OUTGOING_TYPE:
	                dir = 1;
	                break;
	            case CallLog.Calls.INCOMING_TYPE:
	                dir = 0;
	                break;
	            }
	            
	            //Ce ni zgresen ga shranimo
	            if(dir!=-1){
	            	JSONObject podatek=new JSONObject();
	            	
	            	double []location=getLocation();
	    			
	            	try {
						podatek.put("datum", callDate);
						podatek.put("lokacija_x", location[0]);
		    			podatek.put("lokacija_y", location[1]);
		    			podatek.put("tip", "k");
		    			podatek.put("odhodni_dohodni", dir);
		    			podatek.put("stevilka", getOperaterNumber(phNumber));
		    			podatek.put("trajanje", callDuration);
		    			podatek.put("kolicina", 0);
					} catch (JSONException e) {
						showMessage("Napaka (103_a) pri zbiranju ustvarjanju JSONa.");
						e.printStackTrace();
					}
	    			
	    			list.put(podatek);
	            }
	        }
		}catch(Exception e){
			showMessage("Napaka (104) pri zbiranju prometa klicev.");
			e.printStackTrace();
		}
		
	}
	
	private void getSMSMMSData(JSONArray list){
		
		try{
			long lastSendDate=getValue("datum");
			
			Cursor smsSentCursor = managedQuery(Uri.parse("content://sms/sent"), null,"date>?", new String[] {lastSendDate+""}, null);
			
	        int number = smsSentCursor.getColumnIndex("address");
	        int date = smsSentCursor.getColumnIndex("date");
	        
	        while (smsSentCursor.moveToNext()) {
	            
	        	String smsNumber =smsSentCursor.getString(number);
	            long smsDate = Long.valueOf(smsSentCursor.getString(date));
	            
	            packSMSMMSData(list, smsDate, 1, smsNumber, "s");
	            
	        }
	        
	        Cursor smsInboxCursor = managedQuery(Uri.parse("content://sms/inbox"), null,"date>?", new String[] {lastSendDate+""}, null);
			
	        number = smsInboxCursor.getColumnIndex("address");
	        date = smsInboxCursor.getColumnIndex("date");
	        
	        while (smsInboxCursor.moveToNext()) {
	            
	        	String smsNumber =smsInboxCursor.getString(number);
	            long smsDate = Long.valueOf(smsInboxCursor.getString(date));
	            
	            packSMSMMSData(list, smsDate, 0, smsNumber, "s");
	            
	        }
	        
	        Cursor mmsSentCursor = managedQuery(Uri.parse("content://mms/sent"), null,"date>?", new String[] {lastSendDate+""}, null);
			
	        number = mmsSentCursor.getColumnIndex("address");
	        date = mmsSentCursor.getColumnIndex("date");
	        
	        while (mmsSentCursor.moveToNext()) {
	            
	        	String mmsNumber =mmsSentCursor.getString(number);
	            long mmsDate = Long.valueOf(mmsSentCursor.getString(date));
	            
	            packSMSMMSData(list, mmsDate, 1, mmsNumber, "m");
	            
	        }
	        
	        Cursor mmsInboxCursor = managedQuery(Uri.parse("content://mms/inbox"), null,"date>?", new String[] {lastSendDate+""}, null);
			
	        number = mmsInboxCursor.getColumnIndex("address");
	        date = mmsInboxCursor.getColumnIndex("date");
	        
	        while (mmsInboxCursor.moveToNext()) {
	            
	        	String mmsNumber =mmsInboxCursor.getString(number);
	            long mmsDate = Long.valueOf(mmsInboxCursor.getString(date));
	            
	            packSMSMMSData(list, mmsDate, 0, mmsNumber, "m");
	            
	        }
		}catch(Exception e){
			showMessage("Napaka (105) pri zbiranju SMS/MMS prometa.");
			e.printStackTrace();
		}
		
	}
	
	private void packSMSMMSData(JSONArray list, long date, int dir, String number, String tip){
		
		JSONObject podatek=new JSONObject();
		
		double []location=getLocation();
		
    	try {
			podatek.put("datum", date);
			podatek.put("lokacija_x", location[0]);
			podatek.put("lokacija_y", location[1]);
			podatek.put("tip", tip);
			podatek.put("odhodni_dohodni", dir);
			podatek.put("stevilka", getOperaterNumber(number));
			podatek.put("trajanje", 0);
			podatek.put("kolicina", 0);
		} catch (JSONException e) {
			showMessage("Napaka (103_b) pri zbiranju ustvarjanju JSONa.");
			e.printStackTrace();
		}
		
		list.put(podatek);		
		
	}
	
	private void getNETData(JSONArray list){
		
		try{
			long sendBaytes=TrafficStats.getMobileTxBytes();
			long recivedBytes=TrafficStats.getMobileRxBytes();
			
			long totalBytes=sendBaytes+recivedBytes;
			sendBytesNew=totalBytes;
			
			long netOld=getValue("net");
			
			if(netOld <= totalBytes){
				
				totalBytes=totalBytes-netOld;	
			}
			
			long skkb=0;
			
			if(totalBytes>=1024){
				skkb=totalBytes/1024;
			}
			
			
			if(skkb!=0){
				JSONObject podatek=new JSONObject();
				
				double []location=getLocation();
				
				try {
					podatek.put("datum", new Date().getTime());
					podatek.put("lokacija_x", location[0]);
					podatek.put("lokacija_y", location[1]);
					podatek.put("tip", "n");
					podatek.put("odhodni_dohodni", 0);
					podatek.put("stevilka", 0);
					podatek.put("trajanje", 0);
					podatek.put("kolicina", skkb);
				} catch (JSONException e) {
					showMessage("Napaka (103_b) pri zbiranju ustvarjanju JSONa.");
					e.printStackTrace();
				}
				list.put(podatek);
			}
		}catch(Exception e){
			showMessage("Napaka (106) pri zbiranju internetnega prometa.");
			e.printStackTrace();
		}
	}
	
	private void setValue(String name, long value){
		SharedPreferences prefs = this.getPreferences(Context.MODE_PRIVATE);
		
		prefs.edit().putLong(name, value).commit();
	}
	
	private long getValue(String name){
		SharedPreferences prefs = this.getPreferences(Context.MODE_PRIVATE);
		
		return prefs.getLong(name, 0);
	}
	
	private double [] getLocation(){
		double [] location=new double[2];
		location[0]=46.057181;
		location[1]=14.507354;
		
		LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
		Criteria criteria = new Criteria();
		String bestProvider = locationManager.getBestProvider(criteria, true);
		Location locationClass = locationManager.getLastKnownLocation(bestProvider);
		
		if(locationClass!=null){
			location[0]=locationClass.getLatitude();
			location[1]=locationClass.getLongitude();
		}
		
		return location;
	}
	
	private int getOperaterNumber(String n){
		int number=0;
		
		if(n.charAt(0)=='0' && n.charAt(1)!='0'){
			if(n.charAt(2)=='1' || n.charAt(2)=='0' || n.charAt(2)=='8' || n.charAt(2)=='9' || n.charAt(2)=='4'){
				number=Integer.parseInt(n.substring(1, 3));
			}
			else{
				number=Integer.parseInt(n.substring(1, 2));
			}
		}
		else if(n.charAt(0)=='+'){
			if(n.charAt(5)=='1' || n.charAt(5)=='0' || n.charAt(5)=='8' || n.charAt(5)=='9' || n.charAt(5)=='4'){
				number=Integer.parseInt(n.substring(4, 6));
			}
			else{
				number=Integer.parseInt(n.substring(4, 5));
			}
		}
		else if(n.charAt(0)=='0' && n.charAt(1)=='0'){
			if(n.charAt(6)=='1' || n.charAt(6)=='0' || n.charAt(6)=='8' || n.charAt(6)=='9' || n.charAt(6)=='4'){
				number=Integer.parseInt(n.substring(5, 7));
			}
			else{
				number=Integer.parseInt(n.substring(5, 6));
			}
		}
		else if(n.charAt(0)=='8' && n.charAt(1)=='2'){
			number=Integer.parseInt(n.substring(0, 5));
		}
		
		return number;
	}
	
	private void showMessage(String msg){
		Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
	}

}
