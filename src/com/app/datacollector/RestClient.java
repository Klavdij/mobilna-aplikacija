package com.app.datacollector;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.json.JSONObject;

import android.os.AsyncTask;


public class RestClient extends AsyncTask <Void, Void, String>{
	
	ArrayList<NameValuePair> prms;
	
	public void setData(JSONObject podatki, String emso){
		
		prms=new ArrayList<NameValuePair>();
		
		prms.add(new BasicNameValuePair("emso", emso));
		
		prms.add(new BasicNameValuePair("podatki",podatki.toString()));
		
		
	}
	
	protected String getASCIIContentFromEntity(HttpEntity entity)
			throws IllegalStateException, IOException {
		InputStream in = entity.getContent();

		StringBuffer out = new StringBuffer();
		int n = 1;
		while (n > 0) {
			byte[] b = new byte[4096];
			n = in.read(b);

			if (n > 0)
				out.append(new String(b, 0, n));
		}

		return out.toString();
	}

	@Override
	protected String doInBackground(Void... params) {
		HttpClient httpClient = new DefaultHttpClient();
		HttpContext localContext = new BasicHttpContext();
		HttpPost httpPost = new HttpPost("http://sandbox.lavbic.net/stud/iMobi/rest/send/data");
		
		String text = null;
		try {
			httpPost.setEntity(new UrlEncodedFormEntity(prms));
			HttpResponse response = httpClient.execute(httpPost, localContext);
	
			HttpEntity entity = response.getEntity();
	
			text = getASCIIContentFromEntity(entity);

		} catch (Exception e) {
			return e.getLocalizedMessage();
		}

		return text;
	}

}
