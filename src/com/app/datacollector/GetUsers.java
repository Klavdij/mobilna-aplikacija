package com.app.datacollector;

import java.io.IOException;
import java.io.InputStream;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import android.os.AsyncTask;


public class GetUsers extends AsyncTask <Void, Void, String>{
		
	protected String getASCIIContentFromEntity(HttpEntity entity)
			throws IllegalStateException, IOException {
		InputStream in = entity.getContent();

		StringBuffer out = new StringBuffer();
		int n = 1;
		while (n > 0) {
			byte[] b = new byte[4096];
			n = in.read(b);

			if (n > 0)
				out.append(new String(b, 0, n));
		}

		return out.toString();
	}

	@Override
	protected String doInBackground(Void... params) {
		HttpClient httpClient = new DefaultHttpClient();
		HttpGet httpGet = new HttpGet("http://10.0.2.2:8080/Streznik/rest/get/users/data");
		
		String text = null;
		try {
			HttpResponse response = httpClient.execute(httpGet);
	
			HttpEntity entity = response.getEntity();
	
			text = getASCIIContentFromEntity(entity);

		} catch (Exception e) {
			e.getStackTrace();
		}

		return text;
	}

}
